<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Amalan extends Model {

	protected $fillable = array('nama_amalan','deskripsi','jenis','tipe_input','is_built_in','id_kategori');
}
