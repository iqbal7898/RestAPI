<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAmalandipakai extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('amalandipakai', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('target');
			$table->string('repeat');
			$table->string('target_per');
			$table->string('tgl_muncul');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('amalandipakai');
	}

}
