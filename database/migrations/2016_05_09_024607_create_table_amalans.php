<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAmalans extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('amalans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_amalan');
			$table->string('deskripsi');
			$table->string('jenis');
			$table->string('tipe_input');
			$table->string('is_built_in');
			$table->string('id_kategori');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('amalans');
	}

}
