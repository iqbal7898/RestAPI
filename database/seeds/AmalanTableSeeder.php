<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AmalanTableSeeder extends Seeder{
 
	public function run(){
 
		DB::table('amalans')->insert([
			'nama_amalan'=>'solat',
			'deskripsi'=>'solat biasa',
			'jenis'=>'1',
			'tipe_input'=>'1',
			'is_built_in'=>'1',
			'id_kategori'=>'1',
		]);
 
	}
}
